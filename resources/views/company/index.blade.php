@extends('layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Laravel 8 CRUD - Manajemen Company</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-success" href="{{ route('company.create') }}"> Create New Company</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($company as $item)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->alamat }}</td>
            <td>
                <form action="{{ route('company.destroy',$item->id) }}" method="POST">
   
                    <!-- <a class="btn btn-info" href="{{ route('employee.show',$item->id) }}">Show</a> -->
    
                    <a class="btn btn-primary" href="{{ route('company.edit',$item->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    <div class="text-center">
        {!! $company->links() !!}
    </div>
      
@endsection
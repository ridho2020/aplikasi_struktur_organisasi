@extends('layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Company
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('company.update', $company->id) }}" id="myForm">
            @csrf
            @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>                    
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $company->nama }}" aria-describedby="nama" >                
                </div>
                <div class="form-group">
                    <label for="name">Alamat</label>                    
                    <input type="text" name="alamat" class="form-control" id="alamat" value="{{ $company->alamat }}" aria-describedby="alamat" >                
                </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
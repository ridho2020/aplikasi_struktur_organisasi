@extends('layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Laravel 8 CRUD - Manajemen Employee</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-success" href="{{ route('employee.create') }}"> Create New Employee</a>
            </div>
                  <div class="d-flex justify-content-left mb-5">
                    <a href="/download-excell" class="btn btn-sm btn-success rounded-pill mx-2">Download Excell</a>
                    <a href="/download-pdf" class="btn btn-sm btn-success rounded-pill mx-2">Download PDF</a>
                </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Atasan</th>
            <th>Company</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($employee as $item)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->atasan_id }}</td>
            <td>{{ $item->company_id }}</td>
            <td>
                <form action="{{ route('employee.destroy',$item->id) }}" method="POST">
   
                    <!-- <a class="btn btn-info" href="{{ route('employee.show',$item->id) }}">Show</a> -->
    
                    <a class="btn btn-primary" href="{{ route('employee.edit',$item->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    <div class="text-center">
        {!! $employee->links() !!}
    </div>
@endsection
@extends('layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit User
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('employee.update', $employee->id) }}" id="myForm">
            @csrf
            @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>                    
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ $employee->nama }}" aria-describedby="nama" >                
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Pilih Atasan</label>
                        <select name="atasan_id" class="form-control">
                        <option value="1" {{ ($employee->atasan_id == 1) ? 'selected' : '' }}> 1 </option>
                        <option value="2" {{ ($employee->atasan_id == 2) ? 'selected' : '' }}> 2 </option>
                        <option value="3" {{ ($employee->atasan_id == 3) ? 'selected' : '' }}> 3 </option>
                        <option value="4" {{ ($employee->atasan_id == 4) ? 'selected' : '' }}> 4 </option>
                        <option value="5" {{ ($employee->atasan_id == 5) ? 'selected' : '' }}> 5 </option>
                    </select>                     
                </div>
                <div class="form-group">
                     <label for="exampleFormControlSelect1">Pilih Company</label>
                        <select name="company_id" class="form-control">
                        <option value="1" {{ ($employee->company_id == 1) ? 'selected' : '' }}> 1 </option>
                        <option value="2" {{ ($employee->company_id == 2) ? 'selected' : '' }}> 2 </option>
                    </select>                    
                </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
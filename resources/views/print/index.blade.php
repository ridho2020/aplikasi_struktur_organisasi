<style>
table, th, td {
  border: 1px solid black;
}
</style>
<table class="table table-bordered">
    <thead>
      <tr >
        <th scope="col">id</th>
        <th scope="col">Nama</th>
        <th scope="col">Posisi</th>
        <th scope="col">Perusahaan</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($employee as $employee)
        <tr>
        <th scope="row">{{ $employee->id }}</th>
        <td>{{ $employee->nama }}</td>
        <td>{{ $employee->atasan_id }}</td>
        <td>{{ $employee->company_id }}</td>
        </tr>
        @endforeach
    </tbody>
  </table>

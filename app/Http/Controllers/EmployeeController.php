<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Exports\EmployeesExport;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;



class EmployeeController extends Controller
{
    public function index()
    {
        //fungsi eloquent menampilkan data menggunakan pagination
        $employee = Employee::latest()->paginate(5);
        return view('employee.index', compact('employee'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('employee.create');
    }

    public function store(Request $request)
    {
        //melakukan validasi data
        $request->validate([
            'nama' => 'required',
            'jabatan_id' => 'required',
            'company_id' => 'required',
        ]);

        //fungsi eloquent untuk menambah data
        Employee::create($request->all());

        //jika data berhasil ditambahkan, akan kembali ke halaman utama
        return redirect()->route('employee.index')
            ->with('success', 'Employee Berhasil Ditambahkan');
    }

    public function show($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id employee
        $employee = Employee::find($id);
        return view('employee.detail', compact('employee'));
    }

    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id employee untuk diedit
        $employee = Employee::find($id);
        return view('employee.edit', compact('employee'));
    }

    public function update(Request $request, $id)
    {
        //melakukan validasi data
        $request->validate([
            'nama' => 'required',
            'jabatan_id' => 'required',
            'company_id' => 'required',
        ]);

        //fungsi eloquent untuk mengupdate data inputan kita
        Employee::find($id)->update($request->all());

        //jika data berhasil diupdate, akan kembali ke halaman utama
        return redirect()->route('employee.index')
            ->with('success', 'Employee Berhasil Diupdate');
    }

    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        Employee::find($id)->delete();
        return redirect()->route('employee.index')
            ->with('success', 'Employee Berhasil Dihapus');
    }

      public function export()
    {
        return Excel::download(new EmployeesExport, 'employee.xlsx');
    }

    public function DomPDF()
    {
        $employee = Employee::get();
        $pdf = PDF::loadView('print.index', ['employee' => $employee])->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return $pdf->download('employee.pdf');
    }

}
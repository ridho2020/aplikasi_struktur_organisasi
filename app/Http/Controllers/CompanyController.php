<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;


class CompanyController extends Controller
{
    public function index()
    {
        //fungsi eloquent menampilkan data menggunakan pagination
        $company = Company::latest()->paginate(5);
        return view('company.index', compact('company'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        //melakukan validasi data
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
        ]);

        //fungsi eloquent untuk menambah data
        Company::create($request->all());

        //jika data berhasil ditambahkan, akan kembali ke halaman utama
        return redirect()->route('company.index')
            ->with('success', 'Company Berhasil Ditambahkan');
    }

    public function show($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id employee
        $company = Company::find($id);
        return view('company.detail', compact('company'));
    }

    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id employee untuk diedit
        $company = Company::find($id);
        return view('company.edit', compact('company'));
    }

    public function update(Request $request, $id)
    {
        //melakukan validasi data
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
        ]);

        //fungsi eloquent untuk mengupdate data inputan kita
        Company::find($id)->update($request->all());

        //jika data berhasil diupdate, akan kembali ke halaman utama
        return redirect()->route('company.index')
            ->with('success', 'Company Berhasil Diupdate');
    }

    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        Company::find($id)->delete();
        return redirect()->route('company.index')
            ->with('success', 'Company Berhasil Dihapus');
    }
}